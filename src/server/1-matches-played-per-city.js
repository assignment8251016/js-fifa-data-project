function matchesPlayedPerCity(worldCupMatches){
    
    return worldCupMatches.reduce((matchesPerCity, match)=>{
        const city = match['City'].trim();
        if(matchesPerCity[city] === undefined){
            matchesPerCity[city] = 1
        }else{
            matchesPerCity[city] += 1
        }

        return matchesPerCity;
    }, {});
}

module.exports = matchesPlayedPerCity;