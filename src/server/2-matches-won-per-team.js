function matchesWonPerTeam(worldCupMatches){
    const result = {};
    
    return worldCupMatches.reduce((result, match)=>{
        const won = parseInt(match['Home Team Goals'])>parseInt(match['Away Team Goals'])
        const homeTeam = match["Home Team Name"].trim();
        if(result[homeTeam] == undefined)
            result[homeTeam] = 0

        if(won)
            result[homeTeam] += 1;

        return result;
    }, result);
}

module.exports = matchesWonPerTeam;
