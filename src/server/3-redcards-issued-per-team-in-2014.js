function countCharacter(string, char){
    let count=0;
    for(let c of string)
        if (c===char)
            count+=1
    return count;
}

function redCardsIssued(worldCupMatches, worldCupPlayers){

    const matches2014 = worldCupMatches
        .filter(match=>(match['Year']).trim()==="2014");
        
    return worldCupPlayers.reduce((redcard, player)=>{
        const found = matches2014.find(match=>match["MatchID"]===player["MatchID"]);
        if(!(found===undefined)){
            if(player["Event"].includes("R")===true){
                const redcardCount = countCharacter(player["Event"], "R");

                if(redcard[player['Team Initials']] == undefined ){
                    redcard[player['Team Initials']]= redcardCount;
                }else{
                    redcard[player['Team Initials']]+= redcardCount;
                }
            }
        }
        return redcard;
    }, {});

}
module.exports = redCardsIssued;