function count(string, char){
    let count=0;
    for(let c of string)
        if (c===char)
            count+=1
    return count;
}

function top10ScorePlayers(wcpData){

    const playerWithGoals = wcpData.reduce((goalers,player)=>{
        if(player['Event'].includes("G")){
            if (goalers[player['Player Name']] === undefined){
                goalers[player['Player Name']] = count(player['Event'],'G')
            }else{
                goalers[player['Player Name']]+= count(player['Event'],'G')
            }
        }
        return goalers;
    }, {});


    return Object.keys(playerWithGoals).sort((key1, key2)=>
    {
        if(playerWithGoals[key1]>playerWithGoals[key2]){
            return -1;
        }else if(playerWithGoals[key1]<playerWithGoals[key2]){
            return 1;
        }
        return 0;
    }).slice(0,10);

}

module.exports = top10ScorePlayers;