const csvtojson= require("csvtojson")
const fs = require("fs")

//Q1
const matchesPlayedPerCity = require("./1-matches-played-per-city");
csvtojson()
    .fromFile('src/data/WorldCupMatches.csv')
    .then(wcData=>{
        const result = matchesPlayedPerCity(wcData);
        const path = 'src/public/output/1-matches-played-per-city.json'
        fs.writeFileSync(path,JSON.stringify(result));
    });

//Q2
const matchesWonPerTeam = require("./2-matches-won-per-team");
csvtojson()
    .fromFile('src/data/WorldCupMatches.csv')
    .then(wcData=>{
        const result = matchesWonPerTeam(wcData);
        const path = 'src/public/output/2-matches-won-per-team.json'
        fs.writeFileSync(path,JSON.stringify(result));
    });

//Q3
const redCardsIssued = require("./3-redcards-issued-per-team-in-2014")
csvtojson()
    .fromFile('src/data/WorldCupMatches.csv')
    .then((wcMatches)=>{

        csvtojson()
            .fromFile('src/data/WorldCupPlayers.csv')
            .then((wcPlayer=>{
                const result = redCardsIssued(wcMatches, wcPlayer);
                const path = 'src/public/output/3-redcards-issued-per-team-in-2014.json'
                fs.writeFileSync(path,JSON.stringify(result));
                
            }))
    })

//Q4
const top10ScorePlayers = require("./4-top10-players-with-highest-chance-of-scoring-in-match")
csvtojson()
    .fromFile('src/data/WorldCupPlayers.csv')
    .then(wcData=>{
        const result = top10ScorePlayers(wcData);
        const path = 'src/public/output/4-top10-players-with-highest-chance-of-scoring-in-match.json'
        fs.writeFileSync(path,JSON.stringify(result));
    });